const {Telegraf, Scenes} = require('telegraf');
const {CMD_TEXT, URL_API} = require('../config/consts');
const {backMenu} = require('../controllers/commands');
const {mainMenu, authButtonMenu} = require('../utils/buttons');
const axios = require("axios");

// первый шаг сцены
const stepOne = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text;

        ctx.reply('Enter password');
        // указываем state для следующего шага сцены
        ctx.scene.state.new_login = numberText;
        // говорим, чтобы перешёл к следующему шагу
        ctx.wizard.next();
    } catch (error) {
        console.log(error)
        ctx.reply('Oops... An error has occurred.');
        ctx.scene.leave();
    }
});

// второй шаг сцены
const stepTwo = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text

        ctx.reply('Enter the new user\'s phone number in the following format: +998909998877');
        ctx.scene.state.new_password = numberText;

        // выходим со сцены
        ctx.wizard.next();
    } catch (e) {
        ctx.scene.leave();
        console.log(e)
    }
});

// Третий шаг сцены
const stepThree = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text

        ctx.reply('Enter the new user\'s full name in the following format: Nazarov Asadbek Kudratovich');
        ctx.scene.state.new_phone_number = numberText;

        // выходим со сцены
        ctx.wizard.next();
    } catch (e) {
        ctx.scene.leave();
        console.log(e)
    }
});

// Четвертый шаг сцены
const stepFour = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text

        ctx.scene.state.new_full_name = numberText;

        ctx.reply('Please wait...⌛️');
        const response = await axios.post(`${URL_API}/TG/RegisterUser`, {
            login: ctx.scene.state.new_login,
            password: ctx.scene.state.new_password,
            phoneNumber: ctx.scene.state.new_phone_number,
            fullName: ctx.scene.state.new_full_name
        })

        if (response?.status === 200) {
            ctx.replyWithHTML(`You have successfully registered a new user\nlogin: <b>${response?.data?.login}</b>\npassword: <b>${response?.data?.password}</b>\nPhone number:${!!response?.data?.phoneNumber ? "" + `<b>${response?.data?.phoneNumber}</b>` : ""}\nFull name:${!!response?.data?.fullName ? "" + `<b>${response?.data?.fullName}</b>` : ""}`, {
                ...authButtonMenu
            })
        } else {
            ctx.scene.leave();
            throw Error('An error occurred while registering a new user.')
        }
        // выходим со сцены
        ctx.scene.leave();
    } catch (e) {
        ctx.reply(`An authorization error occurred. Code: ${e?.response?.data?.status}. Error description: ${e?.response?.data?.title}. Please try again!`, {
            ...mainMenu
        })
        ctx.scene.leave();
    }
});

// передаём конструктору название сцены и шаги сцен
const registrationScene = new Scenes.WizardScene('registration_wizard', stepOne, stepTwo, stepThree, stepFour)

registrationScene.enter(ctx => ctx.reply('Enter new user login'));

// вешаем прослушку hears на сцену
registrationScene.hears(CMD_TEXT.menu, ctx => {
    ctx.scene.leave();
    return backMenu(ctx);
})

// экспортируем сцену
module.exports = {
    registrationScene
};