const {Telegraf, Scenes} = require('telegraf');
const {CMD_TEXT, URL_API} = require('../config/consts');
const {backMenu} = require('../controllers/commands');
const {mainMenu, authButtonMenu} = require('../utils/buttons');
const axios = require("axios");

// первый шаг сцены
const stepOne = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text
        if (numberText === '/start') {
            return ctx.scene.enter('authorization_wizard');
        }

        ctx.reply('Enter your password')
        // указываем state для следующего шага сцены
        ctx.scene.state.login = numberText;
        // говорим, чтобы перешёл к следующему шагу
        ctx.wizard.next();
    } catch (error) {
        console.log(error)
        ctx.reply('Oops... An error has occurred.');
        ctx.scene.leave();
    }
});

// второй шаг сцены
const stepTwo = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text
        if (numberText === '/start') {
            return ctx.scene.enter('authorization_wizard');
        }

        ctx.scene.state.password = numberText;

        ctx.reply('Please wait...⌛️ Checking your username and password!');
        const response = await axios.post(`${URL_API}/TG/Authenticate?TGUserId=${msg.from.id}&login=${ctx.scene.state.login}&password=${ctx.scene.state.password}`);

        if (response?.status === 200) {
            if (!!response?.data?.isAdmin) {
                ctx.reply(`You have successfully logged in ${response?.data?.login}${!!response?.data?.phoneNumber ? ", " + response?.data?.phoneNumber : ""}!`, {
                    ...authButtonMenu
                })
            } else {
                ctx.reply(`You have successfully logged in ${response?.data?.login}${!!response?.data?.phoneNumber ? ", " + response?.data?.phoneNumber : ""}!`, {
                    ...mainMenu
                })
            }
        } else {
            throw Error('An authorization error has occurred')
        }
        // выходим со сцены
        ctx.scene.leave();
    } catch (e) {
        console.log(e)
        ctx.reply(`An authorization error occurred. Code: ${e?.response?.data?.status}. Error description: ${e?.response?.data?.title}. Please try again!`, {
            ...mainMenu
        })
        ctx.scene.leave();
    }
});

// передаём конструктору название сцены и шаги сцен
const authorizationScene = new Scenes.WizardScene('authorization_wizard', stepOne, stepTwo)

authorizationScene.enter(ctx => ctx.reply('Enter your login'));

// вешаем прослушку hears на сцену
authorizationScene.hears(CMD_TEXT.menu, ctx => {
    ctx.scene.leave();
    return backMenu(ctx);
})

// экспортируем сцену
module.exports = {
    authorizationScene
};