const {Telegraf, Scenes} = require('telegraf');
const {CMD_TEXT, URL_API} = require('../config/consts');
const {backMenu} = require('../controllers/commands');
const {mainMenu, authButtonMenu} = require('../utils/buttons');
const axios = require("axios");

// первый шаг сцены
const stepOne = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text

        ctx.reply('Enter new phone number. In the following format: +998886665544')
        // указываем state для следующего шага сцены
        ctx.scene.state.employ_table_number = numberText;
        // говорим, чтобы перешёл к следующему шагу
        ctx.wizard.next();
    } catch (error) {
        console.log(error)
        ctx.reply('Oops... An error has occurred.');
        ctx.scene.leave();
    }
});

// второй шаг сцены
const stepTwo = Telegraf.on('text', async ctx => {
    try {
        const msg = ctx.message;
        const numberText = msg.text

        ctx.scene.state.new_phone_number = numberText;

        ctx.reply('Please wait...⌛️');
        const response = await axios.post(`${URL_API}/TG/UpdatePhoneNumber`, {
            phoneNumber: numberText,
            login: ctx.scene.state.employ_table_number
        });


        if (response?.status === 200) {
            ctx.replyWithHTML(`You have successfully updated employ phone number\nNew phone number:${!!response?.data?.phoneNumber ? "" + `<b>${response?.data?.phoneNumber}</b>` : ""}\nFull name:${!!response?.data?.fullName ? "" + `<b>${response?.data?.fullName}</b>` : ""}`, {
                ...authButtonMenu
            })
        } else {
            throw Error('An authorization error has occurred')
        }
        // выходим со сцены
        ctx.scene.leave();
    } catch (e) {
        console.log(e)
        ctx.reply(`An authorization error occurred. Code: ${e?.response?.data?.status}. Error description: ${e?.response?.data?.title}. Please try again!`, {
            ...mainMenu
        })
        ctx.scene.leave();
    }
});

// передаём конструктору название сцены и шаги сцен
const changeEmployerPhoneNumberScene = new Scenes.WizardScene('change_employer_phone_number_wizard', stepOne, stepTwo)

changeEmployerPhoneNumberScene.enter(ctx => ctx.reply('Enter employee login(table number).'));

// вешаем прослушку hears на сцену
changeEmployerPhoneNumberScene.hears(CMD_TEXT.menu, ctx => {
    ctx.scene.leave();
    return backMenu(ctx);
})

// экспортируем сцену
module.exports = {
    changeEmployerPhoneNumberScene
};