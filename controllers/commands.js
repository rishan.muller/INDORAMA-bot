// команды для бота 

const {
    mainMenu,
    startCallbackButton
} = require("../utils/buttons");

const start = (ctx) =>
    ctx.reply(`
        Hello, ${ctx.update.message.from.first_name}!`, {
        disable_web_page_preview: true,
        parse_mode: 'HTML',
        ...mainMenu
    });

const backMenu = ctx => {
    ctx.reply(`You are on the menu`, {
        disable_web_page_preview: true,
        parse_mode: 'HTML',
        ...mainMenu
    });
}
const startWhatWeather = ctx => {
    // console.log(ctx)
    /*
    1. Бот запрашивает геопозицию человека в ТГ
    2. Человек по кнопке или через вложения отправляет своё местоположение
    3. Нам приходят даннные и мы отправляет через API запрос по погоде по координатам его
    4. Обработка
    */
    // входим в зарегистрированную в bot.js (15 строка) сцену
    return ctx.scene.enter('weather');
};

const authorization_wizard = ctx => ctx.scene.enter('authorization_wizard');
const change_employer_phone_number_wizard = ctx => ctx.scene.enter('change_employer_phone_number_wizard');
const registration_wizard = ctx => ctx.scene.enter('registration_wizard');
const sendFile = ctx => ctx.scene.enter('sendFile');

const exampleStartCallback = (ctx) =>
    ctx.reply('😳 Sent a message with inline-keyboard and callback button.\nIt leads to the stage "authorization_wizzard"', {
        ...startCallbackButton
    })


module.exports = {
    start,
    backMenu,
    sendFile,
    startWhatWeather,
    authorization_wizard,
    change_employer_phone_number_wizard,
    registration_wizard,
    exampleStartCallback
}