const {Telegraf, Scenes} = require('telegraf');
const {CMD_TEXT, URL_API} = require('../config/consts');
const {backMenu} = require('../controllers/commands');
const axios = require("axios");

// первый шаг сцены
const stepOne = Telegraf.on('document', async ctx => {
    try {
        const document = ctx.message;
        const fileUrl = await ctx.telegram.getFileLink(document.document.file_id)

        if (ctx.message.text === '/start') {
            return ctx.scene.enter('authorization_wizard');
        }

        // указываем state для следующего шага сцены
        ctx.reply(`File (${document.document.file_name?.toString()?.split('.')[0]}) Sending... Please wait⌛️!`)
        const response = await axios.post(`${URL_API}/TG/SendByAdmin2?url=${fileUrl}&fileName=${document.document.file_name}`)
        if (response?.status === 200) {
            ctx.reply(`File (${document.document.file_name}) sent successfully!`)
        }
        // говорим, чтобы перешёл к следующему шагу
        ctx.scene.leave();
    } catch (error) {
        if (error.response.data === 'User not found') {
            const file_name = ctx.message.document.file_name.toString()?.split('.')

            ctx.reply(`❌ Error! No user found with this username(${file_name[0]}) Status: ${error.response.status}`);
        } else {
            ctx.reply(`❌ Error! Unknown error. Status: ${error.response.status}`);
        }
        ctx.scene.leave();
    }
});

// передаём конструктору название сцены и шаги сцен
const sendFileScene = new Scenes.WizardScene('sendFile', stepOne)

sendFileScene.enter(ctx => ctx.reply('Attach file(s). Please note that the file name must be the username of the user you want to send the file to.'));

// вешаем прослушку hears на сцену
sendFileScene.hears(CMD_TEXT.menu, ctx => {
    ctx.scene.leave();
    return backMenu(ctx);
})

// экспортируем сцену
module.exports = {
    sendFileScene
}; 