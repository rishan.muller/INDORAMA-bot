// текстовые команды
const CMD_TEXT = {
    registraion: 'Register new user',
    changeEmployerPhoneNumber: 'Change employer phone number',
    authorization: 'Authorization',
    sendFiles: 'Send files',
    menu: 'Back to main menu'
}

// эжкспортированныые константы проекта свободного доступа
module.exports = {
    URL_API: process.env.API,
    CMD_TEXT
}